# Évaluation - méthodologie de tests
Par Florian LAFUENTE le 22/04/2021

## QCM

Réponses de la question 1 :
- b. une fonction
- d. une classe

Réponses de la question 2 :
- a. Ecrire les tests, déveloper la fonctionnalité jusqu'à ce que tous mes tests passent
- b. Je répète le processus : écriture d'un test, développement, refacto

Réponses de la question 3 : 
- a. Les erreurs levées d'une fonction
- c. Les valeurs retours
- e. Les valeurs limites

Réponses de la question 4 : 
- c. Des valeurs calculées

Réponses de la question 5 :
- b. De simuler le fonctionnement d'une dépendance

