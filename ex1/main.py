def join(list, separator):
    value = ""
    for index, word in enumerate(list):
        value += word
        if index != len(list) - 1:
            value += separator
    return value

def average(list):
    sum = 0
    for number in list:
        sum += number
    return sum / len(list)