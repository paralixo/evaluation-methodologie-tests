import unittest

from ex1.main import join, average


class Ex1TestCase(unittest.TestCase):
    def test_join_list_functional(self):
        value = join(["Hello", "world"], " ")
        self.assertEqual("Hello world", value)

    def test_join_empty_list(self):
        value = join([], " ")
        self.assertEqual("", value)

    def test_join_wrong_type(self):
        with self.assertRaises(TypeError):
            join(5, " ")

    def test_average_functional(self):
        value = average([5, 10, 15])
        self.assertEqual(10, value)

    def test_average_empty_list(self):
        with self.assertRaises(ZeroDivisionError):
            average([])
        
    def test_average_wrong_type(self):
        with self.assertRaises(TypeError):
            average(["Hello"])


if __name__ == '__main__':
    unittest.main()
