import unittest

from ex2.main import *


class LeapTestsV1(unittest.TestCase):
    def test_leap_year_false_not_4_multiple(self):
        value = is_leap_year_v1(2021)
        self.assertFalse(value)

    def test_leap_year_wrong_type(self):
        with self.assertRaises(TypeError):
            is_leap_year_v1("hello world")


class LeapTestsV2(unittest.TestCase):
    def test_leap_year_false_not_4_multiple(self):
        value = is_leap_year_v2(2021)
        self.assertFalse(value)

    def test_leap_year_wrong_type(self):
        with self.assertRaises(TypeError):
            is_leap_year_v2("hello world")

    def test_leap_year_true_400_multiple(self):
        value = is_leap_year_v2(2000)
        self.assertTrue(value)

    def test_leap_year_false_400_multiple(self):
        value = is_leap_year_v2(2020)
        self.assertFalse(value)


class LeapTestsV3(unittest.TestCase):
    def test_leap_year_false_not_4_multiple(self):
        value = is_leap_year_v3(2021)
        self.assertFalse(value)

    def test_leap_year_wrong_type(self):
        with self.assertRaises(TypeError):
            is_leap_year_v3("hello world")

    def test_leap_year_true_400_multiple(self):
        value = is_leap_year_v3(2000)
        self.assertTrue(value)

    def test_leap_year_false_100_multiple_not_400_multiple(self):
        value = is_leap_year_v3(2100)
        self.assertFalse(value)

    def test_leap_year_true_4_multiple_not_100_multiple(self):
        value = is_leap_year_v3(2004)
        self.assertTrue(value)


if __name__ == '__main__':
    unittest.main()
