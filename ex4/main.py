from ex4 import PoolLogic


def start_heating(pool_logic: PoolLogic):
    last_days_temps = pool_logic.get_last_days_temps()
    average_temperature_last_days = sum(last_days_temps)/len(last_days_temps)

    if average_temperature_last_days > 20 and pool_logic.get_actual_temp() > 25:
        pool_logic.set_heater(True)
    else:
        pool_logic.set_heater(False)


