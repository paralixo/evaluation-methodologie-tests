import unittest
from unittest.mock import MagicMock

from ex4.main import *


class PoolTests(unittest.TestCase):
    def test_pool_heating_true(self):
        mock = MagicMock()
        mock.get_last_days_temps.return_value = [18, 20.5, 26]
        mock.get_actual_temp.return_value = 27

        start_heating(mock)

        mock.set_heater.assert_called_with(True)


    def test_pool_heating_false_wrong_actual_temp(self):
        mock = MagicMock()
        mock.get_last_days_temps.return_value = [18, 20.5, 26]
        mock.get_actual_temp.return_value = 23

        start_heating(mock)

        mock.set_heater.assert_called_with(False)


    def test_pool_heating_false_wrong_last_days_temps(self):
        mock = MagicMock()
        mock.get_last_days_temps.return_value = [18, 20.5, 12]
        mock.get_actual_temp.return_value = 27

        start_heating(mock)

        mock.set_heater.assert_called_with(False)


if __name__ == '__main__':
    unittest.main()
