import unittest

from ex3.main import *


class NewLineTestsV1(unittest.TestCase):
    def test_new_line_wrong_line_length_type(self):
        with self.assertRaises(TypeError):
            automatic_new_line_v1("hello world", "line_length")

    def test_new_line_wrong_text_type(self):
        with self.assertRaises(TypeError):
            automatic_new_line_v1(123, 20)

    def test_new_line_empty_text(self):
        value = automatic_new_line_v1("", 20)
        self.assertEqual("", value)


class NewLineTestsV2(unittest.TestCase):
    def test_new_line_wrong_line_length_type(self):
        with self.assertRaises(TypeError):
            automatic_new_line_v2("hello world", "line_length")

    def test_new_line_wrong_text_type(self):
        with self.assertRaises(TypeError):
            automatic_new_line_v2(123, 20)

    def test_new_line_empty_text(self):
        value = automatic_new_line_v2("", 20)
        self.assertEqual("", value)

    def test_new_line_error_word_too_long(self):
        entry_text = "UnImmenseMotQuiFaitPlusDe20Caracteres"
        with self.assertRaises(ValueError):
            automatic_new_line_v2(entry_text, 20)


class NewLineTestsV3(unittest.TestCase):
    def test_new_line_wrong_line_length_type(self):
        with self.assertRaises(TypeError):
            automatic_new_line_v3("hello world", "line_length")

    def test_new_line_wrong_text_type(self):
        with self.assertRaises(TypeError):
            automatic_new_line_v3(123, 20)

    def test_new_line_empty_text(self):
        value = automatic_new_line_v3("", 20)
        self.assertEqual("", value)

    def test_new_line_error_word_too_long(self):
        entry_text = "UnImmenseMotQuiFaitPlusDe20Caracteres"
        with self.assertRaises(ValueError):
            automatic_new_line_v3(entry_text, 20)

    def test_new_line_functional(self):
        entry_text = "J’aimerai découper cette ligne: Cette ligne est beaucoup trop longue alors je souhaite la " \
                     "découper. "
        value = automatic_new_line_v3(entry_text, 20)
        expected_result = "J’aimerai découper\ncette ligne: Cette\nligne est beaucoup\ntrop longue alors je\nsouhaite " \
                          "la\ndécouper. "
        self.assertEqual(expected_result, value)

    def test_new_line_functional_keeping_backslash_n(self):
        entry_text = "J’aimerai découper cette ligne:\nCette ligne est beaucoup trop longue alors je souhaite la " \
                     "découper. "
        value = automatic_new_line_v3(entry_text, 20)
        expected_result = "J’aimerai découper\ncette ligne:\nCette ligne est\nbeaucoup trop longue\nalors je souhaite " \
                          "la\ndécouper. "
        self.assertEqual(expected_result, value)


class NewLineWithSeparatorTestsV1(unittest.TestCase):
    def test_new_line_wrong_line_length_type(self):
        with self.assertRaises(TypeError):
            automatic_new_line_truncate_v1("hello world", "line_length", 20)

    def test_new_line_wrong_text_type(self):
        with self.assertRaises(TypeError):
            automatic_new_line_truncate_v1(123, 20, 20)

    def test_new_line_empty_text(self):
        value = automatic_new_line_truncate_v1("", 20, 20)
        self.assertEqual("", value)

    def test_new_line_error_word_too_long(self):
        entry_text = "UnImmenseMotQuiFaitPlusDe20Caracteres"
        with self.assertRaises(ValueError):
            automatic_new_line_truncate_v1(entry_text, 20, 20)

    def test_new_line_functional(self):
        entry_text = "J’aimerai découper cette ligne: Cette ligne est beaucoup trop longue alors je souhaite la " \
                     "découper. "
        value = automatic_new_line_truncate_v1(entry_text, 20, 20)
        expected_result = "J’aimerai découper\ncette ligne: Cette\nligne est beaucoup\ntrop longue alors je\nsouhaite " \
                          "la\ndécouper. "
        self.assertEqual(expected_result, value)

    def test_new_line_functional_keeping_backslash_n(self):
        entry_text = "J’aimerai découper cette ligne:\nCette ligne est beaucoup trop longue alors je souhaite la " \
                     "découper. "
        value = automatic_new_line_truncate_v1(entry_text, 20, 20)
        expected_result = "J’aimerai découper\ncette ligne:\nCette ligne est\nbeaucoup trop longue\nalors je souhaite " \
                          "la\ndécouper. "
        self.assertEqual(expected_result, value)

    def test_new_line_wrong_truncate_length_type(self):
        with self.assertRaises(TypeError):
            automatic_new_line_truncate_v1(123, 20, "truncate_length")

    def test_new_line_functional_truncate(self):
        entry_text = "J’aimerai découper cette ligne:\nCette ligne est beaucoup trop longue alors je souhaite la " \
                     "découper. "
        value = automatic_new_line_truncate_v1(entry_text, 20, 7)
        expected_result = "J’aimerai découper\ncette ligne:\nCette ligne est bea-\nucoup trop longue\nalors je " \
                          "souhaite la\ndécouper. "
        self.assertEqual(expected_result, value)


if __name__ == '__main__':
    unittest.main()
