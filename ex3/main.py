def automatic_new_line_v1(text, line_length):
    if not isinstance(text, str) or not isinstance(line_length, int):
        raise TypeError("{0} or {1} is invalid".format(text, line_length))

    value = ""
    return value


def automatic_new_line_v2(text, line_length):
    if not isinstance(text, str) or not isinstance(line_length, int):
        raise TypeError("{0} or {1} is invalid".format(text, line_length))

    words = text.split(" ")

    for word in words:
        if len(word) >= line_length:
            raise ValueError("{0} is too long for max line length {1}".format(word, line_length))

    value = ""
    return value


def automatic_new_line_v3(text, line_length):
    if not isinstance(text, str) or not isinstance(line_length, int):
        raise TypeError("{0} or {1} is invalid".format(text, line_length))

    words = text.split(" ")
    value = ""

    for index, word in enumerate(words):
        if len(word) >= line_length:
            raise ValueError("{0} is too long for max line length {1}".format(word, line_length))

        splitted_value = value.split("\n")
        line_length_in_value = len(splitted_value[-1]) if len(splitted_value) > 0 else 0

        # >= pour gerer l'espace qui doit être ajouté
        if line_length_in_value + len(word) >= line_length:
            value += "\n" + word
        else:
            value += (" " if index != 0 else "") + word

    return value


def automatic_new_line_truncate_v1(text, line_length, truncate_length):
    if not isinstance(text, str) or not isinstance(line_length, int) or not isinstance(truncate_length, int):
        raise TypeError("{0} or {1} is invalid".format(text, line_length))

    words = text.split(" ")
    value = ""

    for index, word in enumerate(words):
        if len(word) >= line_length:
            raise ValueError("{0} is too long for max line length {1}".format(word, line_length))

        splitted_value = value.split("\n")
        line_length_in_value = len(splitted_value[-1]) if len(splitted_value) > 0 else 0

        # >= pour gerer l'espace qui doit être ajouté
        if line_length_in_value + len(word) >= line_length:
            length_left = line_length - line_length_in_value - 2
            if length_left >= 1 and len(word) >= truncate_length:
                first_word_part = word[0:length_left]
                second_word_part = word[length_left:]
                value += " " + first_word_part + "-\n" + second_word_part
            else:
                value += "\n" + word
        else:
            value += (" " if index != 0 else "") + word

    return value
